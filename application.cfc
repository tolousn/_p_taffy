<cfcomponent extends="taffy.core.api">
	<cfscript>

		this.mappings['/resources'] = expandPath('./resources');
		this.mappings['/taffy'] = expandPath('./taffy');
		this.name = hash(getCurrentTemplatePath());
		this.datasource = 'demo_sql';

		variables.framework = {};
		variables.framework.debugKey = "debug";
		variables.framework.reloadKey = "reload";
		variables.framework.reloadPassword = "false";
		variables.framework.representationClass = "taffy.core.genericRepresentation";
		variables.framework.returnExceptionsAsJson = true;
		
    variables.framework.reloadOnEveryRequest = true;
		
		function decryptAPI(apiKey){
			/*
			if(listLen(apiKey, '|') gt 1){
				db = listGetAt(apiKey, 1, '|');
				encrypted = replace(apiKey, db & '|', '');
				key = decrypt(encrypted, 'rmx2014');
				today = replace(key, db & '|', '');
				if(today eq dateFormat(now(), 'yyyymmdd')){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
			*/
			if(apiKey eq 'asdf'){
				return true;
			} else {
				return false;
			}
		}
		
		function onApplicationStart(){
			return super.onApplicationStart();
		}

		function onRequestStart(TARGETPATH){
			return super.onRequestStart(TARGETPATH);
		}

		
		
		// this function is called after the request has been parsed and all request details are known
		function onTaffyRequest(verb, cfc, requestArguments, mimeExt){

			if(not structKeyExists(arguments.requestArguments, "apiKey")){
				
				//unauthorized because they haven't included their API key
				return newRepresentation().noData().withStatus(401);
				
			}

			else if(decryptAPI(arguments.requestArguments.apiKey)){
				
				//return JSON
				return true;
			
			}
			
			else {
				
				//unauthorized because the key doesn't match
				return decryptAPI(arguments.requestArguments.apiKey);
				
			}
		}

	</cfscript>
</cfcomponent>
