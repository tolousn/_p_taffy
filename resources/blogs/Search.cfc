<cfcomponent extends="taffy.core.resource" taffy_uri="/blog/search/{count}" hint="Returns list of blog articles">
	<cffunction name="get" access="public" output="false">

		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="count" type="numeric" required="true" hint="Provide number of articles to show" />

		<cfargument name="label" type="numeric" required="false" default="0" hint="Provide a keyword to search for within a title or subtitle." />
		<cfargument name="month_year" type="string" required="false" default="" hint="Provide month and year combination from which to extract articles. (4-2014)" />
    
		<cfif label neq ''>
			<cfquery name="preq">
				SELECT post_id
				FROM blog_posts2labels
				WHERE label_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#label#" />
			</cfquery>
		</cfif>
    
		<cfquery name="q">
			SELECT TOP #count# author, body, caption, date_create, id, image, title
		  	FROM blog_posts
			WHERE 1 = <cfqueryparam cfsqltype="cf_sql_integer" value="1" />
			<cfif label neq 0>AND id IN (#valueList(preq.post_id)#)</cfif>
			<cfif month_year neq ''>
				<cfif listLen(month_year,'-') eq 2>
				<cfset pMonth = listGetAt(month_year,1,'-')>
				<cfset pYear = listGetAt(month_year,2,'-')>
				<cfif pMonth eq 12><cfset nMonth = 1><cfset nYear = pYear + 1><cfelse><cfset nMonth = pMonth + 1><cfset nYear = pYear></cfif>
				<cfif pMonth lt 10><cfset pMonth = '0' & pMonth></cfif>
				<cfif nMonth lt 10><cfset nMonth = '0' & nMonth></cfif>
				AND date_create > '#pYear#-#pMonth#-01 00:00:00' 
				AND date_create < '#nYear#-#nMonth#-01 00:00:00' 
			  </cfif>
			</cfif>
		  	ORDER BY date_create DESC
		</cfquery>
					
		<cfreturn representationOf(q).withStatus(200) />
  </cffunction>   
</cfcomponent>