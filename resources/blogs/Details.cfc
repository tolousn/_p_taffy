<cfcomponent extends="taffy.core.resource" taffy_uri="/blogs/{id}" hint="Returns specific blog">
	<cffunction name="get" access="public" output="false">

		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT author, body, caption, date_create, id, image, title
			FROM blog_posts
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>