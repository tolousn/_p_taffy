<cfcomponent extends="taffy.core.resource" taffy_uri="/blogs/labels" hint="Returns list of all blog labels">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">

		<cfquery name="q">
			SELECT id, label
			FROM blog_labels
			ORDER BY id
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>