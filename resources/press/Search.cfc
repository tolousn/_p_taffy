<cfcomponent extends="taffy.core.resource" taffy_uri="/press/search/{count}" hint="Returns list of press article">
	<cffunction name="get" access="public" output="false">
		<!--- display settings --->
		<cfargument name="count" type="numeric" required="true" hint="Provide number of articles to show" />
		<cfargument name="keyword" type="string" required="false" hint="Provide a keyword to search for within a title or subtitle." />
		<cfargument name="month_year" type="string" required="false" hint="Provide month and year combination from which to extract articles. (4-2014)" />
    
    	<cfquery name="q">
			SELECT TOP #count# author, body, date_create, date_publish, filename, id, publicationname, sub_title, title, CASE WHEN type = 1 THEN 'Article' ELSE 'Press Release' END type, url
      		FROM press
			WHERE status = <cfqueryparam cfsqltype="cf_sql_integer" value="1" />
      		<cfif isDefined('keyword')>AND (title LIKE '%#keyword#%' OR sub_title LIKE '%#keyword#%')</cfif>
        	<cfif isDefined('month_year')>
				<cfif listLen(month_year,'-') eq 2>
					<cfset pMonth = listGetAt(month_year,1,'-')>
					<cfset pYear = listGetAt(month_year,2,'-')>
					<cfif pMonth eq 12><cfset nMonth = 1>
						<cfset nYear = pYear + 1>
					<cfelse>
						<cfset nMonth = pMonth + 1>
						<cfset nYear = pYear>
					</cfif>
					<cfif pMonth lt 10><cfset pMonth = '0' & pMonth></cfif>
					<cfif nMonth lt 10><cfset nMonth = '0' & nMonth></cfif>
					AND date_create > '#pYear#-#pMonth#-01 00:00:00' 
					AND date_create < '#nYear#-#nMonth#-01 00:00:00' 
			  	</cfif>
        	</cfif>
			ORDER BY date_create DESC
  		</cfquery>
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>