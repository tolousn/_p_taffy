<cfcomponent extends="taffy.core.resource" taffy_uri="/alerts" hint="Inserts new alert into the database">

	<cffunction name="put" access="public" output="false">

		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
			<cfargument name="first" type="string" required="true" default="" hint="Provide user's first name" />
			<cfargument name="last" type="string" required="true" default="" hint="Provide user's last name" />
			<cfargument name="type" type="numeric" required="true" default="1" hint="Provide property type (1 - Sales, 2 - Rentals)" />
			<cfargument name="email" type="string" required="true" default="" hint="Provide user's email address" />
			<cfargument name="phone" type="string" required="false" default="" hint="Provide user's phone number" />
			<cfargument name="ext" type="string" required="false" default="" hint="Provide user's phone extension" />
			<cfargument name="cat" type="string" required="false" default="" hint="Provide specific neighborhood id(s) [CSV]" />
			<cfargument name="priceMin" type="numeric" required="false" default="0" hint="Provide minimum price" />
			<cfargument name="priceMax" type="numeric" required="false" default="100000000" hint="Provide maximum price" />
			<cfargument name="bedsMin" type="numeric" required="false" default="0" hint="Provide minimum bedroom count" />
			<cfargument name="bathMin" type="numeric" required="false" default="0" hint="Provide minimum bathroom count" />
			<cfargument name="comments" type="string" required="false" default="" hint="Provide additional comments" />
			<cfargument name="moveDate" type="string" required="false" default="" hint="Provide user's move date" />
			<cfargument name="amenities" type="string" required="false" default="" hint="Provide amenities limitation [CSV] (doorman / elevator / healtClub / furnished / laundry / hardwood / nofee)" />

			<cfquery name="q">
				INSERT INTO alerts 
				(
					email,
					firstName,
					lastName,
					phone,
					ext,
					AlertType,
					status,
					cat,
					minPrice,
					maxPrice,
					beds, bath,
					Comment,
					OccDate,
					alertMethod,  
					<cfif isDefined('amenities')>
						<cfloop list="#amenities#" index="A">
							#A#, 
						</cfloop>
					</cfif>
					contact_id
				) 
				VALUES 
				(
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#email#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#first#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#last#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#phone#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#ext" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#type#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="1" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#cat#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#priceMin#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#priceMax#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#bedsMin#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#bathMin#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#comments#" />,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#moveDate#" />,
					<cfqueryparam cfsqltype="cf_sql_integer" value="1" />,
					<cfif isDefined('amenities')>
						<cfloop list="#amenities#" index="A">
							<cfqueryparam cfsqltype="cf_sql_integer" value="1" />, 
						</cfloop>
					</cfif>
					<cfqueryparam cfsqltype="cf_sql_integer" value="0" />
				)
			</cfquery>

			<cfreturn noData().withStatus(200) />
	</cffunction>
</cfcomponent>