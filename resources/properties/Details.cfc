<cfcomponent extends="taffy.core.resource" taffy_uri="/properties/{id}" hint="Returns specific listing">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT p.id main_id,
			CASE WHEN p.hideaddress <> 1 THEN p.house + ' ' + p.address + ' ' + p.apt + ' ' + c.cat_name ELSE p.location + ' ' + c.cat_name END main_address, 
			CASE WHEN p.status = 1 THEN 'For Sale' 
			WHEN p.status = 2 THEN 'For Rent' 
			WHEN p.status = 11 THEN 'In Contract' 
			WHEN p.status = 12 THEN 'Offer In' 
			WHEN p.status = 21 THEN 'App. Pending'
			WHEN p.status = 19 THEN 'Sold' 
			WHEN p.status = 22 THEN 'Rented' END main_status, 

			p.imgcount main_images, 
			p.imgphoto main_image, 
			p.features main_description, 

			c.cat_name main_neighborhood, 

			p.type essentials_type, 
			p.beds essentials_beds, 
			p.bath essentials_bath,  
			p.rooms essentials_rooms, 
			p.size essentials_size, 
			p.units essentials_units, 
			p.vtour essentials_video,
			p.date_update essentials_date, 
			p.available essentials_available, 
			p.building_id essentials_building, 
			p.floor essentials_floor,
			p.subwayline essentials_subway, 

			CASE WHEN pets = 0 THEN 'Unknown'
			WHEN pets = 1 THEN 'Cats Only'
			WHEN pets = 2 THEN 'Small Dogs'
			WHEN pets = 3 THEN 'Pets OK'
			WHEN pets = 4 THEN 'Dogs Only'
			WHEN pets = 98 THEN 'Case By Case'
			WHEN pets = 99 THEN 'No Pets' END essentials_pets,

			CASE WHEN p.openHouse > 0 AND p.openHouseEnd > DATEADD(hour, -1, getdate()) THEN p.openHouse + '-' + p.openHouseEnd
			WHEN p.openHouse2 > 0 AND p.openHouseEnd2 > DATEADD(hour, -1, getdate()) THEN p.openHouse2 + '-' + p.openHouseEnd2
			WHEN p.isohbyappt > 0 THEN 'By Appointment Only' END essentials_openhouse,


			a1.id agents_agent_id, 
			a1.firstName + ' ' + a1.lastName agents_agent_name, 
			a1.email agents_agent_email,
			CASE WHEN a1.mobile <> '' THEN a1.mobile ELSE a1.phone END agents_agent_phone,
			a1.image agents_agent_image,
			a2.id agents_coagent_id, 
			a2.firstName + ' ' + a2.lastName agents_coagent_name, 
			a2.email agents_coagent_email,
			CASE WHEN a2.mobile <> '' THEN a2.mobile ELSE a2.phone END agents_coagent_phone,
			a2.image agents_coagent_image,

			p.price financials_price, 
			p.fee financials_fee, 
			p.down financials_downpayment, 
			p.maintanance financials_maintenance, 
			p.taxes financials_taxes, 

			p.featured extras_featured, 
			p.furnished extras_furnished, 
			p.newconstruction extras_developments, 
			p.nofee extras_nofee, 
			CASE WHEN ( ( p.openHouse > 0 AND p.openHouseEnd > DATEADD(hour, -1, getdate()) ) OR ( p.openHouse2 > 0 AND p.openHouseEnd2 > DATEADD(hour, -1, getdate()) ) OR ( p.isohbyappt > 0  ) ) THEN 1 ELSE 0 END extras_openhouse
			FROM properties p 
			LEFT JOIN cats c ON p.cat = c.cat_id
			LEFT JOIN agents a1 ON p.contact = a1.id
			LEFT JOIN agents a2 ON p.contact2 = a2.id
			WHERE p.id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>
	
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>