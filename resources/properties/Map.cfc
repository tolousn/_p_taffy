<cfcomponent extends="taffy.core.resource" taffy_uri="/properties/map/{id}" hint="Returns specific listing's map information">

	<cffunction name="get" access="public" output="false">	
  
    <!--- required --->
    <cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" type="numeric" required="true" hint="Provide id of the property" />
    
		<!--- get all IDs --->
		<cfquery name="q">
    	SELECT hideAddress, imgphoto, showaddressas, latitude, longitude 
      FROM properties p 
      	LEFT JOIN buildings b ON p.building_id = b.id 
      WHERE hideaddress = 0 
      	AND p.id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
        AND latitude IS NOT NULL
        AND latitude <> ''
        AND latitude <> 0
    </cfquery>
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>


</cfcomponent>