<cfcomponent extends="taffy.core.resource" taffy_uri="/buildings/photos/{id}" hint="Returns specific building's photos">
	<cffunction name="get" access="public" output="false">	
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" type="numeric" required="true" hint="Provide id of the building" />

		<!--- get all IDs --->
		<cfquery name="q">
			SELECT name, filename, sort 
			FROM images 
			WHERE building_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>
    
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>