<cfcomponent extends="taffy.core.resource" taffy_uri="/buildings/{id}" hint="Returns specific building">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT id, name, house, address, location, city, state, zip, public_note, contact, cat, status, type, propertyType, built,
			era, stories, units, families, bldgsize, lotsize, ext, minrent, maxrent, minbuy, maxbuy, beds, price, maintanance, taxes,
			exp, incrent, down, deduct, common, featured, fee, op, pets, new, nofee, elevator, doorman, brownstone, healthclub, pool,
			garage, subway, newconstruction, subwayline, openhouse, openhouseend, openhouse2, openhouseend2, style,  website, phone,
			laundry, bicycleroom, storage, nursery, lounge, valet, parking, concession, roofdeck, heating, cooling, wifi, keys,
			wheelchairaccess, commonoutdoorspace, receivingroom, businesscenter, virtualdoorman, gas, electricity, heat, water,
			imgcount, cableinternet, ac, greenbuilding, freightelevator, concierge, newdevelopment, highspeedinternet, maidservice,
			childrenplayroom, courtyard, driveway 
			FROM buildings 
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>
    
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>