<cfcomponent extends="taffy.core.resource" taffy_uri="/links/categories" hint="Returns list of all link categories">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">

		<cfquery name="q">
			SELECT cat_id id, cat_name category, sort
			FROM linksCats
			ORDER BY sort
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
 	</cffunction>   
</cfcomponent>