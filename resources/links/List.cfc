<cfcomponent extends="taffy.core.resource" taffy_uri="/links" hint="Returns list of all links">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">

		<cfquery name="q">
			SELECT company_name company, url, contact_person contact, phone, email, image, cat_name category
			FROM links l 
			LEFT JOIN LinksCats c ON l.cat = c.cat_id 
			WHERE status = <cfqueryparam cfsqltype="cf_sql_integer" value="1" /> 
			ORDER BY c.sort, l.sort
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>