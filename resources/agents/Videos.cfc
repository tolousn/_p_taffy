<cfcomponent extends="taffy.core.resource" taffy_uri="/agents/videos/{id}" hint="Returns specific agent's videos">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT id, title, embed, image, sort, date_create, description
			FROM videos
			WHERE agent_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />      
			ORDER BY sort, date_create DESC    
		</cfquery>    

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>