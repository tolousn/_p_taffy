<cfcomponent extends="taffy.core.resource" taffy_uri="/agents/{id}" hint="Returns specific agent">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />
			
		<cfquery name="q">
			SELECT bio, email, fax, firstname, id, image, lang, lastname, mobile, office, phone, quote, title,
			social_f link_facebook,
			social_g link_google,
			social_i link_instagram,
			social_l link_linkedin,
			social_p link_pinterest,
			social_t link_twitter,
			social_y link_youtube,
			CASE WHEN link_1 <> '' THEN '<a href=' + link_1 + ' target=_blank>' + link_name_1 + '</a>' END link_personal_1,
			CASE WHEN link_2 <> '' THEN '<a href=' + link_2 + ' target=_blank>' + link_name_2 + '</a>' END link_personal_2,
			CASE WHEN link_3 <> '' THEN '<a href=' + link_3 + ' target=_blank>' + link_name_3 + '</a>' END link_personal_3
			FROM agents 
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
			AND status in (-1,1)
		</cfquery>
			
		<cfreturn representationOf(q).withStatus(200) />
  	</cffunction>   
</cfcomponent>