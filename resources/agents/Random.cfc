<cfcomponent extends="taffy.core.resource" taffy_uri="/agents/random/{count}" hint="Returns random list of agents">
	<cffunction name="get" access="public" output="false">
  	
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="count" type="numeric" required="true" hint="Provide number of agents to show" />
		
		<!--- get all IDs --->
		<cfquery name="q">
			SELECT TOP #count# id, firstname, lastname, title, email, phone, mobile, fax, bio, quote, lang language, image, office, social_f link_facebook, social_g link_google, social_i link_instagram, social_l link_linkedin, social_p link_pinterest, social_t link_twitter, social_y link_youtube
			FROM agents
			WHERE status IN (-1,1)       
			ORDER BY newId()    
		</cfquery>  
		
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>