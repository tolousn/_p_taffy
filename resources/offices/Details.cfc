<cfcomponent extends="taffy.core.resource" taffy_uri="/offices/{id}" hint="Returns specific office">
	<cffunction name="get" access="public" output="false">

		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT id, name, address, city, state, zip, phone, fax, logo, email
			FROM offices
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>