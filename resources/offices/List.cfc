<cfcomponent extends="taffy.core.resource" taffy_uri="/offices" hint="Returns list of all offices">
	<cffunction name="get" access="public" output="false">
		<cfargument name="apiKey" type="string" required="true">
    
		<cfquery name="q">
			SELECT id, name, address, city, state, zip, phone, fax, logo, email
			FROM offices 
			ORDER BY id
		</cfquery>
    
		<cfreturn representationOf(q).withStatus(200) />
 	</cffunction>   
</cfcomponent>