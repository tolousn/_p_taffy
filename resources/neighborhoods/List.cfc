<cfcomponent extends="taffy.core.resource" taffy_uri="/neighborhoods" hint="Returns list of all neighborhoods">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">

		<cfquery name="q">
			SELECT cat_id, cat_name, parent, text_1, sort, img, imgName 
			FROM cats 
			ORDER BY sort
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>