<cfcomponent extends="taffy.core.resource" taffy_uri="/neighborhoods/{id}" hint="Returns specific neighborhood">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT cat_id, cat_name, parent, text_1, sort, img, imgName
			FROM cats 
			WHERE cat_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>